<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Value;

class Subject extends Model
{

    protected $table = 'subject';

    protected $fillable = [
        'subject_type_id'
    ];

    public function value()
    {
        return $this->belongsTo('App\Value', 'subject_type_id');
    }
}
