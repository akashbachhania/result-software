<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\SubjectRequest;
use App\Subject;
use App\Value;
use View;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allSubjects = Subject::where('is_deleted','=',0)->get();
        return View('subject.index', compact('allSubjects'))->with('title','Subject');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Value::where('key_id', '2')->orderBy('title')->lists('title', 'id');
        return View('subject.create', compact('items'))->with('title','Add Subject');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectRequest $requestData)
    {
        $subject = new Subject;
        $subject->code            =   $requestData->code;
        $subject->name            =   $requestData->name;
        $subject->exam_code       =   $requestData->exam_code;
        $subject->subject_type_id =   $requestData->subject_type_id;
        $subject->created_by      =   Auth::user()->id;
        $subject->updated_by      =   Auth::user()->id;
        $subject->save();
        $requestData->session()->flash('alert-success', 'subject was successful added!');
        return redirect('/subject');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = Subject::find($id);
        $items = Value::where('key_id', '2')->orderBy('title')->lists('title', 'id');
        return View::make('subject.edit', compact('items'))
            ->with('subject', $subject); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubjectRequest $requestData, $id)
    {
        $subject = Subject::find($id);
        $subject->code           =   $requestData->code;
        $subject->name           =   $requestData->name;
        $subject->exam_code    =   $requestData->exam_code;
        $subject->subject_type_id =   $requestData->subject_type_id;
        $subject->updated_by     =   Auth::user()->id;
        $subject->save();
        $requestData->session()->flash('alert-success', 'subject was successful edited!');
        return redirect('/subject');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = Subject::find($id);
        $subject->is_deleted = 1;
        $subject->save();
        
        return redirect('/subject');
    }
}
