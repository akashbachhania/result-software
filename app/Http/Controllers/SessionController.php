<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\SessionRequest;
use App\Session;
use App\Value;
use View;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allSessions = Session::where('is_deleted','=',0)->get();
        return View('session.index', compact('allSessions'))->with('title','Session');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('session.create')->with('title','Add Session');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SessionRequest $requestData)
    {
        $session = new Session;
        $session->session_id      =   $requestData->session_id;
        $session->name            =   $requestData->name;
        $session->status          =   $requestData->status;
        $session->created_by      =   Auth::user()->id;
        $session->updated_by      =   Auth::user()->id;
        $session->save();
        $requestData->session()->flash('alert-success', 'Session was successful added!');
        return redirect('/session');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $session = Session::find($id);
 
        return View::make('session.edit')
            ->with('session', $session); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SessionRequest $requestData, $id)
    {
        $session = Session::find($id);
        $session->session_id      =   $requestData->session_id;
        $session->name            =   $requestData->name;
        $session->status          =   $requestData->status;
        $session->updated_by      =   Auth::user()->id;
        $session->save();
        $requestData->session()->flash('alert-success', 'Session was successful edited!');
        return redirect('/session');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $session = Session::find($id);
        $session->is_deleted = 1;
        $session->save();
        return redirect('/session');
    }
}
