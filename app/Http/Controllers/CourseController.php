<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\CourseRequest;
use App\Course;
use View;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCourses = Course::where('is_deleted','=',0)->get();
        return View('course.index', compact('allCourses'))->with('title','Course'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('course.create')->with('title','Add Course'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $requestData)
    {
        //Insert Query
        $course = new Course;
        $course->code           =   $requestData->code;
        $course->name           =   $requestData->name;
        $course->description    =   $requestData->description;
        $course->course_type_id =   $requestData->course_type_id;
        $course->no_of_terms    =   $requestData->no_of_terms;

        $course->min_no_of_terms            =   $requestData->min_no_of_terms;
        $course->min_no_of_credits          =   $requestData->min_no_of_credits;
        $course->min_GGPA                   =   $requestData->min_GGPA;
        $course->min_SGPA                   =   $requestData->min_SGPA;
        $course->min_completion_period      =   $requestData->min_completion_period;
        $course->max_completion_period      =   $requestData->max_completion_period;
        $course->no_of_core_elective_subjects    =   $requestData->no_of_core_elective_subjects;
        $course->no_of_laboratory           =   $requestData->no_of_laboratory;
        $course->no_project_work            =   $requestData->no_project_work;

        $course->created_by     =   Auth::user()->id;
        $course->updated_by     =   Auth::user()->id;
        $course->save();
        $requestData->session()->flash('alert-success', 'Course was successful added!');
        return redirect('/course');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the course
        $course = Course::find($id);
        // show the edit form and pass the course
        return View::make('course.edit')
            ->with('course', $course); 

        /*$name = Input::get('name');
        $course = Course::find($id);
        $course->$name = Input::get('value');
        $course->save(); */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $requestData, $id)
    {
        //Update Query
        $course = Course::find($id);
        $course->code           =   $requestData->code;
        $course->name           =   $requestData->name;
        $course->description    =   $requestData->description;
        $course->course_type_id =   $requestData->course_type_id;
        $course->no_of_terms    =   $requestData->no_of_terms;

        $course->min_no_of_terms            =   $requestData->min_no_of_terms;
        $course->min_no_of_credits          =   $requestData->min_no_of_credits;
        $course->min_GGPA                   =   $requestData->min_GGPA;
        $course->min_SGPA                   =   $requestData->min_SGPA;
        $course->min_completion_period      =   $requestData->min_completion_period;
        $course->max_completion_period      =   $requestData->max_completion_period;
        $course->no_of_core_elective_subjects    =   $requestData->no_of_core_elective_subjects;
        $course->no_of_laboratory           =   $requestData->no_of_laboratory;
        $course->no_project_work            =   $requestData->no_project_work;
        
        $course->updated_by     =   Auth::user()->id;
        $course->save();
        $requestData->session()->flash('alert-success', 'Course was successful edited!');
        return redirect('/course');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        $course->is_deleted = 1;
        $course->save();
        //$course->session()->flash('alert-success', 'Course was successfully deleted!');
        return redirect('/course');
    }

}
