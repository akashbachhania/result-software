<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\BranchRequest;
use App\Branch;
use View;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allBranches = Branch::where('is_deleted','=',0)->get();
        return View('branch.index', compact('allBranches'))->with('title','Branch'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('branch.create')->with('title','Add Branch');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $requestData)
    {
        $branch                 =   new Branch;
        $branch->code           =   $requestData->code;
        $branch->name           =   $requestData->name;
        $branch->created_by     =   Auth::user()->id;
        $branch->updated_by     =   Auth::user()->id;
        $branch->save();
        $requestData->session()->flash('alert-success', 'Branch was successful added!');
        return redirect('/branch');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get the branch
        $branch = Branch::find($id);
        // show the edit form and pass the course
        return View::make('branch.edit')
            ->with('branch', $branch);
        /*$name = Input::get('name');
        $branch = Branch::find($id);
        $branch->$name = Input::get('value');
        $branch->save();*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BranchRequest $requestData, $id)
    {
        $branch                 =   Branch::find($id);
        $branch->code           =   $requestData->code;
        $branch->name           =   $requestData->name;
        $branch->updated_by     =   Auth::user()->id;
        $branch->save();
        $requestData->session()->flash('alert-success', 'Branch was successful updated!');
        return redirect('/branch');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::find($id);
        $branch->is_deleted = 1;
        $branch->save();
        //$course->session()->flash('alert-success', 'Course was successfully deleted!');
        return redirect('/branch');
    }
}
