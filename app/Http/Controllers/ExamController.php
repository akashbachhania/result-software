<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\ExamRequest;
use App\Exam;
use App\Value;
use View;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allExams = Exam::where('is_deleted','=',0)->get();
        return View('exam.index', compact('allExams'))->with('title','Exam Month System');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $items = Value::where('key_id', '2')->orderBy('title')->lists('title', 'id');
        return View('exam.create')->with('title','Add Exam Month');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExamRequest $requestData)
    {
        $exam = new Exam;
        $exam->month_id        =   $requestData->month_id;
        $exam->month           =   $requestData->month;
        $exam->status          =   $requestData->status;
        $exam->created_by      =   Auth::user()->id;
        $exam->updated_by      =   Auth::user()->id;
        $exam->save();
        $requestData->session()->flash('alert-success', 'Exam Month was successfully added!');
        return redirect('/exam');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Exam::find($id);
        // $items = Value::where('key_id', '2')->orderBy('title')->lists('title', 'id');
        return View::make('exam.edit')->with('exam', $exam); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ExamRequest $requestData, $id)
    {
        $exam = Exam::find($id);
        $exam->month_id          =   $requestData->month_id;
        $exam->month             =   $requestData->month;
        $exam->status            =   $requestData->status;
        $exam->updated_by     =   Auth::user()->id;
        $exam->save();
        $requestData->session()->flash('alert-success', 'Exam Month was successful edited!');
        return redirect('/exam');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam = Exam::find($id);
        $exam->is_deleted = 1;
        $exam->save();
        
        return redirect('/exam');
    }
}
