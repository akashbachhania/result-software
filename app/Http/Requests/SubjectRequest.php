<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Subject;

class SubjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method == 'POST'){
            return [
                'code' => 'required|unique:subject',
                'name' => 'required',
                'exam_code' => 'required|unique:subject',
                'subject_type_id' => 'required',
            ];
        }else{
            $id = $this->get('id');
            $code = $this->input('code');
            $name = $this->input('name');
            $codeUnique = Subject::where(compact('id','code')) -> exists() ? '' : 'unique:subject';
            $examCodeUnique = Subject::where(compact('id','exam_code')) -> exists() ? '' : 'unique:subject';
            return [
                'code' => 'required|'.$codeUnique,
                'name' => 'required',
                'exam_code' => 'required|'.$examCodeUnique,
                'subject_type_id' => 'required',
            ];
        }
    }
}
