<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

use App\Session;

class SessionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method == 'POST'){
            return [
                'session_id' => 'required|unique:session',
                'name' => 'required',
                "status"        => 'required',
            ];
        }else{
            $id = $this->get('id');
            $sessionId = $this->input('session_id');echo $sessionId;die;
           echo  Session::where(compact('id','session_id')) -> exists() ? '' : 'unique:session';die;
            $sessionIdUnique = Session::where(compact('id','session_id')) -> exists() ? '' : 'unique:session';
            return [
                'session_id'    => 'required|'.$sessionIdUnique,
                'name'          => 'required',
                "status"        => 'required',
            ];
        }
    }
}
