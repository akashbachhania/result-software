<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Course;

class CourseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method == 'POST'){
            $validation = [
                'code' => 'required|unique:course',
            ];
        }else{
            $id = $this->get('id');
            $code = $this->input('code');
            $codeUnique = Course::where(compact('id','code')) -> exists() ? '' : 'unique:course';
            $validation = [
                'code' => 'required|'.$codeUnique,
            ];
        }

        $validation += [
            'name' => 'required',
            'no_of_terms' => 'required|integer|min:1',
            'min_no_of_terms' => 'required',
            'min_no_of_credits' => 'required',
            'min_GGPA' => 'required',
            'min_SGPA' => 'required',
            'min_completion_period' => 'required',
            'max_completion_period' => 'required',
            'no_of_core_elective_subjects' => 'required',
            'no_of_laboratory' => 'required',
            'no_project_work' => 'required',
        ];

        return $validation;
    }
}
