<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Branch;

class BranchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method == 'POST'){
            $validation = [
                'code' => 'required|unique:branch',
            ];
            $validation += [
                'name' => 'required',
            ];
            return $validation;
        }else{
            $id = $this->get('id');
            $code = $this->input('code');
            $codeUnique = Branch::where(compact('id','code')) -> exists() ? '' : 'unique:branch';
            return [
                'code' => 'required|'.$codeUnique,
                //'code' => 'required|unique:branch,id,'.$this->get('id'),
                'name' => 'required',
            ];
        }
    }
}
