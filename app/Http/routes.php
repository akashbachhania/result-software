<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Auth\AuthController@getLogin');

/* User Authentication */
Route::get('users/login', 'Auth\AuthController@getLogin');
Route::post('users/login', 'Auth\AuthController@postLogin');
Route::get('users/logout', 'Auth\AuthController@getLogout');

Route::get('users/register', 'Auth\AuthController@getRegister');
Route::post('users/register', 'Auth\AuthController@postRegister');

/* Authenticated users */
Route::group(['middleware' => 'auth'], function()
{
    Route::resource('course', 'CourseController');
    Route::get('course/edit/{id}','CourseController@edit');
    Route::get('course/delete/{id}','CourseController@destroy');

    Route::resource('branch', 'BranchController');
    Route::get('branch/edit/{id}','BranchController@edit');
    Route::get('branch/delete/{id}','BranchController@destroy');

    Route::resource('subject','SubjectController');
    Route::get('subject/edit/{id}','SubjectController@edit');
    Route::get('subject/delete/{id}','SubjectController@destroy');

    Route::resource('session','SessionController');
    Route::get('session/edit/{id}','SessionController@edit');
    Route::get('session/delete/{id}','SessionController@destroy');

    Route::resource('exam','ExamController');
    Route::get('exam/edit/{id}','ExamController@edit');
    Route::get('exam/delete/{id}','ExamController@destroy');
});