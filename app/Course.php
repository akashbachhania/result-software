<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Value;

class Course extends Model
{


    protected $table = 'course';

    protected $fillable = [
        'course_type_id'
    ];

    public function value()
    {
        return $this->belongsTo('App\Value', 'course_type_id');
    }
}
