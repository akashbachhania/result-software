<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Value extends Model {

	protected $table = 'value';

    public function course()
    {
        return $this->hasMany('App/Course');
    }
	
}