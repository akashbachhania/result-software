<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 20);
            $table->string('name', 100);
            $table->longText('description')->nullable();
            $table->integer('course_type_id')->unsigned();
            $table->foreign('course_type_id')->references('id')->on('value');
            $table->integer('no_of_terms');
            $table->integer('min_no_of_terms');
            $table->integer('min_no_of_credits');
            $table->float('min_GGPA');
            $table->float('min_SGPA');
            $table->integer('min_comlpetion_period');
            $table->integer('max_completion_period');
            $table->integer('no_of_core_elective_subjects');
            $table->integer('no_of_laboratory');
            $table->integer('no_project_work');
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->integer('created_by')->unsigned()->nullable()   ;
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('course');
    }
}
