<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 50);
            $table->string('middle_name', 50);
            $table->string('last_name', 50);
            $table->string('fathers_name', 100);
            $table->string('fathers_contact_no', 20);
            $table->string('mothers_name', 100);
            $table->string('mothers_occupation', 100);
            $table->string('mothers_contact_no', 20);
            $table->date('dob');
            $table->string('gender', 6);
            $table->integer('address_id')->unsigned();
            $table->foreign('address_id')->references('id')->on('address');
            $table->string('email', 50);
            $table->string('phone', 20);
            $table->string('guardian_name', 100);
            $table->integer('guardian_address_id');
            $table->string('JEE_roll_no', 10);
            $table->string('enrollment_no', 20);
            $table->integer('AIR');
            $table->integer('quota_id')->unsigned();
            $table->foreign('quota_id')->references('id')->on('value');//Foreign key to Value table. Key will be "Quota"
            $table->string('roll_no_10', 10);
            $table->string('marks_10', 10);
            $table->string('marksheet_no_10', 10);
            $table->string('roll_no_12', 10);
            $table->string('marks_12', 10);
            $table->string('marksheet_no_12', 10);
            $table->string('samagra', 10);
            $table->string('aadhar', 20);
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('value');//Foreign key to Value table. Key will be "Category"
            $table->integer('branch_id');
            $table->integer('religion_id')->unsigned();
            $table->foreign('religion_id')->references('id')->on('value');//Foreign key to Value table. Key will be "Religion"
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student');
    }
}
