<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentSchemeSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_scheme_subject', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->integer('scheme_structure_id')->unsigned();
            $table->foreign('scheme_structure_id')->references('id')->on('scheme_structure');
            $table->integer('subject_id')->unsigned();
            $table->foreign('subject_id')->references('id')->on('subject');
            $table->integer('lecture_credit')->default(0);
            $table->integer('tutorial_credit')->default(0);
            $table->integer('practical_credit')->default(0);
            $table->integer('other_credits')->default(0)->nullable();
            $table->integer('grade_id')->unsigned();
            $table->foreign('grade_id')->references('id')->on('grade');
            $table->integer('status')->default(1);
            $table->integer('is_deleted')->default(0);
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->integer('updated_by')->unsigned();
            $table->foreign('updated_by')->references('id')->on('users');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_scheme_subject');
    }
}
