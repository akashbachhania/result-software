<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number', 5);
            $table->string('street', 255);
            $table->string('locality', 255);
            $table->string('city', 100);
            $table->string('state', 50);
            $table->string('country', 50);
            $table->string('pincode', 10);
            $table->integer('is_deleted')->default(0);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::drop('address');
    }
}
