<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Key;
use App\Value;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        $this->call('KeyTableSeeder');
        $this->call('ValueTableSeeder');
        $this->call('UserTableSeeder');
        // Model::reguard();
    }
}
class KeyTableSeeder extends Seeder 
{
   public function run()  
    {
        $titleArray = array(
                            'Course Type',
                            'Subject Type',
                            'Quota',
                            'Category',
                            'Religion',
                            'Class Groups',
                            'Student Status',
                            'Valuation Type',
                            );
        foreach ($titleArray as $key => $value)
        {
           DB::table('key')->insert(
                [
                'title' => $value,
                ]);
        }
    }
} 

class UserTableSeeder extends Seeder 
{
   public function run()  
    {
        DB::table('users')->insert(
        [
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => '$2y$10$58Zva6S1IcJwuUVdsD4yweyqnJ155EOXqayQmeTD6cEZTS.pzTYPK'    
        ]);
    }
} 

class ValueTableSeeder extends Seeder 
{
   public function run()  
    {
        $keyIdArray = array('1','1','2','2','2','2','2','2','2','3','3',
        '4','4','4','4','4','5','5','5','5','5','5','5','6','6','7',
        '7','8','8');

        $titleArray = array('Year',
                        'Semester',
                        'Programme Core',
                        'Programme Elective',
                        'Open Elective',
                        'Other Course',
                        'Seminar',
                        'Project',
                        'Viva',
                        'All India',
                        'State(Madhya Pradesh)',
                        'SC',
                        'ST',
                        'OBS',
                        'General',
                        'JKR',
                        'Hindu',
                        'Muslim',
                        'Jain',
                        'Christian',
                        'Sikh',
                        'Buddh',
                        'Other',
                        'AB',
                        'BA',
                        'Regular',
                        'Regular Repeat',
                        'Internal Valuation',
                        'External Valuation'
        );
        for($i=0; $i<= count($titleArray)-1;$i++)
        {
           DB::table('value')->insert(
                [
                'key_id' => $keyIdArray[$i],
                'title' => $titleArray[$i],
                ]);
        }
        
        
    }
}              