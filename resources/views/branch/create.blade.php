@extends('master')
@section('content')
<div class="container-fluid" ng-controller="courseController" >
    <div class="row">
      <div class="col-md-4 col-md-push-4">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
         </div> <!-- end .flash-message -->
      </div>
    </div>
    <div class="row" >

    <div class="col-md-4 col-md-push-4">
      {!! Form::open(['action' => 'BranchController@store', 'class' => 'form']) !!}

      <fieldset ng-form="courseEntry">

          @if($errors->any())
            <ul class="alert alert-danger">
              @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
            </ul>
          @endif

	        <div class="form-group">
	            <label for="course_code">Branch Code *</label>
              {!! Form::text('code', null, ['class'=>'form-control', 'placeholder'=>'Branch Code', 'required']) !!}
	        </div>
        
	        <div class="form-group">
	            <label for="course_name">Branch Name *</label>
              {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Branch Name', 'required']) !!}
	        </div>

          {!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
      </fieldset>
      {!! Form::close() !!}

    </div>

    </div>
  </div>

@endsection