@extends('master')
@section('content')
<div class="container-fluid" ng-controller="courseController" >
    
     <div class="row">
      <div class="col-md-8 col-md-push-2">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
         </div> <!-- end .flash-message -->    
      </div>
    </div>
    <div class="row" >
      <div class="col-md-8 col-md-push-2 table_form">
        <form>
          <table id="branch_table" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Branch Code</th>
                <th>Branch Name</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody >
              @foreach( $allBranches as $branch )
                <tr>
                  <input type="hidden" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" id="url{{ $branch->id }}" value="branch/edit/{{ $branch->id }}">
                  <td>{{ $branch->code }}</td>
                  <td>{{ $branch->name }}</td>
                  <td>
                    <a href="{{ url('/branch/edit/'.$branch->id) }}" class="btn btn-danger btn-mini">Edit</a> 
                    <a href="{{ url('/branch/delete/'.$branch->id) }}" class="btn btn-danger btn-mini" onclick="return confirm('Are you sure?')">Delete</a>
                  </td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>
@endsection