@extends('master')
@section('content')
<div class="container-fluid" ng-controller="courseController" >
    <div class="row">
      <div class="col-md-8 col-md-push-2">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
        </div> <!-- end .flash-message -->
      </div>
    </div>
    <div class="row" >
      <div class="col-md-8 col-md-push-2 table_form">
        <form id="form-id" action="{{ action('CourseController@edit') }}" method="patch">
          <table id="course_table" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Code</th>
                <th>Course Name</th>
                <th>Term Type</th>
                <th>No. of Terms</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody >
              @foreach( $allCourses as $course )
                <tr>
                  <input type="hidden" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" id="url{{ $course->id }}" value="course/edit/{{ $course->id }}">
                  <td>{{ $course->code }}</td>
                  <td>{{ $course->name }}</td>
                  <td>{{ $course->value->title }}</td>
                  <td>{{ $course->no_of_terms }}</td>
                  <td>
                    <a href="{{ url('/course/edit/'.$course->id) }}" class="btn btn-danger btn-mini">Edit</a> 
                    <a href="{{ url('/course/delete/'.$course->id) }}" class="btn btn-danger btn-mini" onclick="return confirm('Are you sure?')">Delete</a>
                  </td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>
@endsection