@extends('master')
@section('content')
<div class="container-fluid" ng-controller="courseController" >
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
      @endforeach
     </div> <!-- end .flash-message -->
    <div class="row" >
      <div class="col-md-6 col-md-push-1 table_form">
        <form id="form-id" action="{{ action('CourseController@edit') }}" method="patch">
          <table id="course_table" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Code</th>
                <th>Course Name</th>
                <th>Year/Semester</th>
                <th>No. of terms</th>
              </tr>
            </thead>

            <tbody >
              @foreach( $allCourses as $course )
                <tr>
                  <input type="hidden" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" id="url{{ $course->id }}" value="course/edit/{{ $course->id }}">
                  <td><input type="text" value = "{{ $course->code }}" name="code" class="update" id="{{ $course->id }}"></td>
                  <td><input type="text" value = "{{ $course->name }}" name="name" class="update" id="{{ $course->id }}"></td>
                  <td>{{ $course->value->title }}</td>
                  <td><input type="text" value = "{{ $course->no_of_terms }}" name="no_of_terms" class="update" id="{{ $course->id }}"></td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </form>
    </div>


    <div class="col-md-3 col-md-push-2">
      {!! Form::open(['action' => 'CourseController@store', 'class' => 'form']) !!}

      <fieldset ng-form="courseEntry">

          @if($errors->any())
            <ul class="alert alert-danger">
              @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
            </ul>
          @endif

	        <div class="form-group">
	            <label for="course_code">Course Code</label>
              {!! Form::text('code', null, ['class'=>'form-control', 'placeholder'=>'Course Code']) !!}
	        </div>
        
	        <div class="form-group">
	            <label for="course_name">Course Name</label>
              {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Course Name']) !!}
	        </div>
        
          <div class="form-group">
            <label for="course_description">Course Description</label>
            {!! Form::textarea('description', null,  ['class'=>'form-control', 'placeholder'=>'Course Description']) !!}
          </div>

          <div class="row"> 
            <h4 class=" text-center">Course Yearly/Semester</h4>    
        
            <div class="col-md-4 text-center check">
	             <label>
                  Year
	             </label>
               {!! Form::radio('course_type_id', '1', ['checked' => 'checked']) !!}
            </div>

            <div class="col-md-4 text-center">
               <label>
                  Semester
               </label>
               {!! Form::radio('course_type_id', '2') !!} 
           </div>
          </div>

          <div class="form-group terms">
            <label for="terms">No. of terms</label>
              {!! Form::text('no_of_terms', null, ['class'=>'form-control', 'placeholder'=>'Terms']) !!}
              <label for="terms">Semester</label> 
          </div>
          {!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
      </fieldset>
      {!! Form::close() !!}

    </div>

    </div>
  </div>

@endsection