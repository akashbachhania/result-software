@extends('master')
@section('content')
<div class="container-fluid" ng-controller="courseController" >
    <div class="row">
      <div class="col-md-6 col-md-push-3">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
         </div> <!-- end .flash-message -->
      </div>
    </div>
    <div class="row" >
      <div class="col-md-6 col-md-push-3">
        {!! Form::model($course, array('route' => array('course.update', $course->id), 'method' => 'PUT')) !!}

        <fieldset ng-form="courseEntry">

            @if($errors->any())
              <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif

  	        <div class="form-group">
  	            <label for="course_code">Course Code*</label>
                {!! Form::hidden('id', $course->id) !!}
                {!! Form::text('code', null, ['class'=>'form-control', 'placeholder'=>'Course Code']) !!}
  	        </div>
          
  	        <div class="form-group">
  	            <label for="course_name">Course Name*</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Course Name']) !!}
  	        </div>
          
            <div class="form-group">
              <label for="course_description">Course Description</label>
              {!! Form::textarea('description', null,  ['class'=>'form-control', 'placeholder'=>'Course Description']) !!}
            </div>

            <div class="row"> 
              <h4 class=" text-center">Course Yearly/Semester*</h4>    
          
              <div class="col-md-4 text-center check">
  	             <label>
                    Year
  	             </label>
                 {!! Form::radio('course_type_id', '1', ['checked' => 'checked']) !!}
              </div>

              <div class="col-md-4 text-center">
                 <label>
                    Semester
                 </label>
                 {!! Form::radio('course_type_id', '2') !!} 
             </div>
            </div>

            <div class="form-group terms">
              <label for="terms">No. of Terms*</label>
                {!! Form::text('no_of_terms', null, ['class'=>'form-control', 'placeholder'=>'Terms']) !!}
                <label for="terms">Semester</label> 
            </div>

            <div class="form-group terms">
              <label for="terms">Min No. of Terms*</label>
                {!! Form::text('min_no_of_terms', null, ['class'=>'form-control', 'placeholder'=>'Min No. of Terms']) !!}
            </div>

            <div class="form-group terms">
              <label for="terms">Min No. of Credits*</label>
                {!! Form::text('min_no_of_credits', null, ['class'=>'form-control', 'placeholder'=>'Min No. of Credits']) !!}
            </div>

            <div class="form-group terms">
              <label for="terms">Min GGPA*</label>
                {!! Form::text('min_GGPA', null, ['class'=>'form-control', 'placeholder'=>'Min GGPA']) !!}
            </div>

            <div class="form-group terms">
              <label for="terms">Min SGPA*</label>
                {!! Form::text('min_SGPA', null, ['class'=>'form-control', 'placeholder'=>'min_SGPA']) !!}
            </div>

            <div class="form-group terms">
              <label for="terms">Min Completion Period*</label>
                {!! Form::text('min_completion_period', null, ['class'=>'form-control', 'placeholder'=>'Min Completion Period']) !!}
            </div>

            <div class="form-group terms">
              <label for="terms">Max Completion Period*</label>
                {!! Form::text('max_completion_period', null, ['class'=>'form-control', 'placeholder'=>'Max Completion Period']) !!}
            </div>

            <div class="form-group terms">
              <label for="terms">No. of Core Elective Subjects*</label>
                {!! Form::text('no_of_core_elective_subjects', null, ['class'=>'form-control', 'placeholder'=>'No. of Core Elective Subjects']) !!}
            </div>

            <div class="form-group terms">
              <label for="terms">No. of Laboratory*</label>
                {!! Form::text('no_of_laboratory', null, ['class'=>'form-control', 'placeholder'=>'No. of Laboratory']) !!}
            </div>

            <div class="form-group terms">
              <label for="terms">No. of Porject Work*</label>
                {!! Form::text('no_project_work', null, ['class'=>'form-control', 'placeholder'=>'No. of Porject Work']) !!}
            </div>
            
            {!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
        </fieldset>
        {!! Form::close() !!}

      </div>
    </div>
  </div>

@endsection