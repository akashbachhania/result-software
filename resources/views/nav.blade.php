<nav class="navbar">
  <div class="container-fluid">

      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>  
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="collapse-1">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Course <span class="caret"></span></a>

              <ul class="dropdown-menu">
                <li><a href="{{ url('/course') }}">Show Course</a></li>
                <li><a href="{{ url('/course/create') }}">Add Course</a></li>
                <li><a href="{{ url('/users/logout') }}">Logout</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Branch <span class="caret"></span></a>

              <ul class="dropdown-menu">
                <li><a href="{{ url('/branch') }}">Show Branch</a></li>
                <li><a href="{{ url('/branch/create') }}">Add Branch</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Subject <span class="caret"></span></a>

              <ul class="dropdown-menu">
                <li><a href="{{ url('/subject') }}">Show Subject</a></li>
                <li><a href="{{ url('/subject/create') }}">Add Subject</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Session <span class="caret"></span></a>

              <ul class="dropdown-menu">
                <li><a href="{{ url('/session') }}">Show Session</a></li>
                <li><a href="{{ url('/session/create') }}">Add Session</a></li>
              </ul>
            </li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Exam Month <span class="caret"></span></a>

              <ul class="dropdown-menu">
                <li><a href="{{ url('/exam') }}">Show Exam Month</a></li>
                <li><a href="{{ url('/exam/create') }}">Add Exam Month</a></li>
              </ul>
            </li>
          </ul>
      </div><!-- /.navbar-collapse -->

    </div><!-- /.container-fluid -->
</nav>
