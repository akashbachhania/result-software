<!DOCTYPE html>

<html>

<head>

  <title>{{ $title }}</title>

  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/css/main.css')}}">
</head>
<body>

<div class="container-fluid">
    <div class="row">

      <div class="col-md-3 col-md-push-4" id="login">
        {!! Html::ul($errors->all(), array('class'=>'alert alert-danger errors')) !!}

        {!! Form::open(array('url' => 'users/login','class'=>'form')) !!}
          
          <div class="form-group">
            
            <label for="user_name">Email</label>

            {!! Form::email('email', null, array('class' => 'form-control','placeholder' => 'example@domain.com', 'required')) !!}
          
          </div>
          
          <div class="form-group">
          
            <label for="pwd">Password</label>

            {!! Form::password('password', array('class' => 'form-control', 'required')) !!}
          
          </div>

          {!! Form::submit('Sign In' , array('class' => 'btn btn-success')) !!}
        
        {!! Form::close() !!}

      </div>

    </div>
  </div>

</body>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
      $(".dropdown-toggle").dropdown();
  });
</script>

</html>