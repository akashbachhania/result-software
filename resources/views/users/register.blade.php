<!DOCTYPE html>

<html>

<head>

  <title>Exam System</title>

  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{asset('assets/css/main.css')}}">
</head>
<body>

<div class="row">
    <div class="col-md-6">
        <h2>Register to create an account</h2>
        <p>Hi, here you can create a new account. Just fill in the form and press sign up button.</p>
        <br>
        {!! Html::ul($errors->all(), array('class'=>'errors')) !!}

        {!! Form::open(array('url' => 'users/register','class'=>'form')) !!}
        
        <br>{!! Form::label('first_name', 'First Name') !!}
        {!! Form::text('first_name', null, array('class' => 'form-control','placeholder' => 'kenny')) !!}

        <br>{!! Form::label('last_name', 'Second Name') !!}
        {!! Form::text('last_name', null, array('class' => 'form-control','placeholder' => 'kenny')) !!}
        
        <br>{!! Form::label('email', 'E-Mail Address') !!}
        {!! Form::text('email', null, array('class' => 'form-control','placeholder' => 'example@gmail.com')) !!}
        
        <br>{!! Form::label('password', 'Password') !!}
        {!! Form::password('password', array('class' => 'form-control')) !!}
        
        <br>
        {!! Form::label('password_confirmation','Confirm Password',['class'=>'control-label']) !!}
        {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
        
        <br>
        {!! Form::submit('Sign Up' , array('class' => 'btn btn-primary')) !!}
        
        {!! Form::close() !!}
        <br>
    </div>
</div>
</body>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
      $(".dropdown-toggle").dropdown();
  });
</script>

</html>