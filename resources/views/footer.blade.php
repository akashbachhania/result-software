<script type="text/javascript">

	$(document).ready(function(){

		if(document.getElementById("terms")){
			var course_type_id = $('input[name=course_type_id]:checked').val();
			if(course_type_id == 1)
				$('.terms label:last-child').text('Year');
			else
				$('.terms label:last-child').text('Semester');
		}
		
	});

</script>