@extends('master')
@section('content')
<div class="container-fluid" ng-controller="subjectController" >
    <div class="row">
      <div class="col-md-6 col-md-push-3">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
        </div> <!-- end .flash-message -->    
      </div>
    </div>
    
    <div class="row" >
      <div class="col-md-6 col-md-push-3">
        {!! Form::open(['action' => 'SessionController@store', 'class' => 'form']) !!}

        <fieldset ng-form="subjectEntry">

            @if($errors->any())
              <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif

  	        <div class="form-group">
  	            <label for="subject_code">Session Id *</label>
                {!! Form::text('session_id', null, ['class'=>'form-control', 'placeholder'=>'Session Name','required']) !!}
  	        </div>
          
  	        <div class="form-group">
  	            <label for="subject_name">Session Name *</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Session Name','required']) !!}
  	        </div>

            <div class="row"> 
              <h4 class=" text-center">Session Status *</h4>    
          
              <div class="col-md-4 text-center check check_1">
                 <label>
                    Active
                 </label>
                 {!! Form::radio('status', '1', ['checked' => 'checked']) !!}
              </div>

              <div class="col-md-4 text-center check_2">
                 <label>
                    InActive
                 </label>
                 {!! Form::radio('status', '0') !!} 
             </div>
            </div>
          
            
            {!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
        </fieldset>
        {!! Form::close() !!}

      </div>
    </div>
  </div>

@endsection