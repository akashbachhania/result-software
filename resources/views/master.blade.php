<!DOCTYPE html>
<html>
    <head>
        @include('header')
    </head>
    <body ng-app="davv">
        @include('nav')
        @yield('content')
    </body>
@include('footer')
</html>