@extends('master')
@section('content')
<div class="container-fluid" ng-controller="examController" >
     <div class="row">
      <div class="col-md-8 col-md-push-2">  
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
        </div> <!-- end .flash-message -->
      </div>
     </div>
    <div class="row" >
      <div class="col-md-8 col-md-push-2 table_form">
        <form id="form-id" action="{{ action('ExamController@edit') }}" method="patch">
          <table id="subject_table" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Month Id</th>
                <th>Month</th>
                <th>Month Status</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach( $allExams as $exam )
                <tr>
                  <input type="hidden" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" id="url{{ $exam->id }}" value="exam/edit/{{ $exam->id }}">
                  <td>{{ $exam->month_id }}</td>
                  <td>{{ $exam->month }}</td>
                  <td>{{ $exam->status == 1?'Active':'In Active' }}</td>
                  <td>
                    <a href="{{ url('/exam/edit/'.$exam->id) }}" class="btn btn-danger btn-mini">Edit</a> 
                    <a href="{{ url('/exam/delete/'.$exam->id) }}" class="btn btn-danger btn-mini" onclick="return confirm('Are you sure you want to delete this record?')">Delete</a>
                  </td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>
@endsection