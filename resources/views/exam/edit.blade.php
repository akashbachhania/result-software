@extends('master')
@section('content')
<div class="container-fluid" ng-controller="examController" >
    <div class="row">
      <div class="col-md-6 col-md-push-3">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
         </div> <!-- end .flash-message -->
      </div>
    </div>  
    <div class="row" >
      <div class="col-md-6 col-md-push-3">
        {!! Form::model($exam, array('route' => array('exam.update', $exam->id), 'method' => 'PUT')) !!}

        <fieldset ng-form="examEntry">

            @if($errors->any())
              <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif

  	        <div class="form-group">
  	            <label for="month_id">Month Id</label>
                {!! Form::hidden('id', $exam->id) !!}
                {!! Form::text('month_id', null, ['class'=>'form-control', 'placeholder'=>'Month Id']) !!}
  	        </div>
          
  	        <div class="form-group">
  	            <label for="month">Month</label>
                {!! Form::text('month', null, ['class'=>'form-control', 'placeholder'=>'Month']) !!}
  	        </div>
          
            <div class="row"> 
              <h4 class="text-center">Month Status</h4>    
          
              <div class="col-md-4 text-center check check_1">
                 <label>
                    Active
                 </label>
                 {!! Form::radio('status', '1', ['checked' => 'checked']) !!}
              </div>

              <div class="col-md-4 text-center check_2">
                 <label>
                    In Active
                 </label>
                 {!! Form::radio('status', '0') !!} 
             </div>
            </div>
            
            
            {!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
        </fieldset>
        {!! Form::close() !!}

      </div>
    </div>
  </div>

@endsection