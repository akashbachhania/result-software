@extends('master')
@section('content')
<div class="container-fluid" ng-controller="subjectController" >
    <div class="row">
      <div class="col-md-6 col-md-push-3">
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
        </div> <!-- end .flash-message -->    
      </div>
    </div>
    
    <div class="row" >
      <div class="col-md-6 col-md-push-3">
        {!! Form::open(['action' => 'SubjectController@store', 'class' => 'form']) !!}

        <fieldset ng-form="subjectEntry">

            @if($errors->any())
              <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
              </ul>
            @endif

  	        <div class="form-group">
  	            <label for="subject_code">Subject Code *</label>
                {!! Form::text('code', null, ['class'=>'form-control', 'placeholder'=>'Subject Code','required']) !!}
  	        </div>
          
  	        <div class="form-group">
  	            <label for="subject_name">Subject Name *</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Subject Name','required']) !!}
  	        </div>
          
            <div class="form-group">
              <label for="exam_code">Sub Exam Code *</label>
              {!! Form::text('exam_code', null,  ['class'=>'form-control', 'placeholder'=>'Sub Exam Code','required']) !!}
            </div>

            <div class="form-group">
              <label for="exam_code">Subject Type *</label>
              {!! Form::select('subject_type_id', $items, 3, ['class' => 'form-control'],'required') !!}
            </div>
            
            {!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
        </fieldset>
        {!! Form::close() !!}

      </div>
    </div>
  </div>

@endsection