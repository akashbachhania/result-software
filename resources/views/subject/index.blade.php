@extends('master')
@section('content')
<div class="container-fluid" ng-controller="subjectController" >
     <div class="row">
      <div class="col-md-8 col-md-push-2">  
        <div class="flash-message">
          @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

              <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
          @endforeach
        </div> <!-- end .flash-message -->
      </div>
     </div>
    <div class="row" >
      <div class="col-md-8 col-md-push-2 table_form">
        <form id="form-id" action="{{ action('SubjectController@edit') }}" method="patch">
          <table id="subject_table" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Sub Code</th>
                <th>Subject Name</th>
                <th>Type</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody >
              @foreach( $allSubjects as $subject )
                <tr>
                  <input type="hidden" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" id="url{{ $subject->id }}" value="subject/edit/{{ $subject->id }}">
                  <td>{{ $subject->code }}</td>
                  <td>{{ $subject->name }}</td>
                  <td>{{ $subject->value->title }}</td>
                  <td>
                    <a href="{{ url('/subject/edit/'.$subject->id) }}" class="btn btn-danger btn-mini">Edit</a> 
                    <a href="{{ url('/subject/delete/'.$subject->id) }}" class="btn btn-danger btn-mini" onclick="return confirm('Are you sure?')">Delete</a>
                  </td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </form>
    </div>
  </div>
</div>
@endsection