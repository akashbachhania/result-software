@extends('master')
@section('content')
<div class="container-fluid">
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
      @endforeach
     </div> <!-- end .flash-message -->
    
    <div class="row">

        <div class="col-md-4 col-md-push-1 table_form1">
            <form id="form-id" action="{{ action('CourseController@edit') }}" method="patch">
                <table id="subject_table" class="table table-bordered" cellspacing="0" width="100%">      
                    <thead>
                        <tr>
                            <th>Sub Code</th>
                            <th>Subject Name</th>
                            <th>Type</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach( $allSubjects as $subjects )    
                        <tr>
                          <input type="hidden" id="token" value="{{ csrf_token() }}">
                          <input type="hidden" id="url{{ $course->id }}" value="course/edit/{{ $course->id }}">
                          <td><input type="text" value = "{{ $course->code }}" name="code" class="update" id="{{ $course->id }}"></td>
                          <td><input type="text" value = "{{ $course->name }}" name="name" class="update" id="{{ $course->id }}"></td>
                          <td>{{ $course->value->title }}</td>
                          <td><input type="text" value = "{{ $course->no_of_terms }}" name="no_of_terms" class="update" id="{{ $course->id }}"></td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </form>

        </div>

            <div class="col-md-3 col-md-push-2">
                
                <form>
                  
                  <div class="form-group">
                    
                    <label for="sub_code">Subject Code</label>
                    
                    <input type="text" class="form-control" id="sub_code" placeholder="Subject Code">
                  
                  </div>
                  
                  <div class="form-group">
                  
                    <label for="sub_name">Subject Name</label>
                  
                    <input type="text" class="form-control" id="sub_name" placeholder="Subject name">
                  
                  </div>
                  
                  <div class="form-group">
                  
                    <label for="sub_exam_code">Sub Exam Code</label>
                  
                    <input type="text" class="form-control" id="sub_exam_code" placeholder="Sub Exam Code">
                  
                  </div>
                  

                  <div class="row"> 
                      <h4 class=" text-center">Subject Type</h4>        
                      <div class="col-md-5 text-center check">

                           <label>
                        
                              <input type="checkbox" checked="checked"> Compulsory

                           </label>
                        
                      </div>

                      <div class="col-md-4 text-center">
                        
                           <label>
                        
                              <input type="checkbox" checked="checked"> Elective

                           </label>

                      </div>

                  </div>

                  <button type="submit" class="btn btn-success">Submit</button>
                
                </form>

            </div>

        </div>
    </div>
@endsection