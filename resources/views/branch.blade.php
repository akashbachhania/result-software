@extends('master')
@section('content')
<div class="container-fluid" ng-controller="courseController" >
    <div class="flash-message">
      @foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))

        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
        @endif
      @endforeach
     </div> <!-- end .flash-message -->
    <div class="row" >
      <div class="col-md-6 col-md-push-1 table_form">
        <form>
          <table id="branch_table" class="display table table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Branch</th>
                <th>Branch Name</th>
              </tr>
            </thead>

            <tbody >
              @foreach( $allBranches as $branch )
                <tr>
                  <input type="hidden" id="token" value="{{ csrf_token() }}">
                  <input type="hidden" id="url{{ $branch->id }}" value="branch/edit/{{ $branch->id }}">
                  <td><input type="text" value = "{{ $branch->code }}" id="{{ $branch->id }}" name="code" class="update"></td>
                  <td><input type="text" value = "{{ $branch->name }}" id="{{ $branch->id }}" name="name" class="update"></td>
                </tr>
              @endforeach
          </tbody>
        </table>
      </form>
    </div>


    <div class="col-md-3 col-md-push-2">
      {!! Form::open(['action' => 'BranchController@store', 'class' => 'form']) !!}

      <fieldset ng-form="courseEntry">

          @if($errors->any())
            <ul class="alert alert-danger">
              @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
            </ul>
          @endif

	        <div class="form-group">
	            <label for="course_code">Branch Code</label>
              {!! Form::text('code', null, ['class'=>'form-control', 'placeholder'=>'Branch Code']) !!}
	        </div>
        
	        <div class="form-group">
	            <label for="course_name">Branch Name</label>
              {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Branch Name']) !!}
	        </div>

          {!! Form::submit( 'Submit', ['class'=>'btn btn-success']) !!}
      </fieldset>
      {!! Form::close() !!}

    </div>

    </div>
  </div>

@endsection